# omv

Documentation for the OpenMediaVault operating system.

## Steps
WIP

```text
 1. Décompresser l'archive .xz
 2. Ecrire le .img sur la carte SD
 3. Attendre que l'installation soit terminée
 4. Installer les mises à jour
 5. Eteindre le système et récupérer la carte SD
 6. Agrandir la partition système
 7. Créer une partition de swap
 8. Modifier le config.txt pour ajouter max_usb_current=1
 9. Rédémarrer

10. Activer et utiliser la partition de swap
11. Activer les repository supplémentaires
12. Rechercher les plugins et mises à jours
13. General Settings
	1. Auto logout: Disabled
	2. Change the Web password
14. Date & Time
	1. Time zone: Europe/Zurich
15. Network
	1. Hostname: OMV-Corsier
	2. Ajouter une interface réseau et la configurer
		1. Check if everything is fine in /etc/network/interfaces
	3. Service Discovery: Activer que le nécessaire: SMB/NFS
16. Notifications
	1. 
17. Power Management
	1. Enable monitoring
	2. Power button: Shutdown
	3. Scheduled job: Nothing
18. Monitoring
	1. Activate the monitoring
19. Certificates
	1. 
20. Scheduled Jobs
21. Update Management
	1. Enable community updates
	2. Verify for updates
	3. Install updates
22. Plugins
	 1. Remove Apple
	 2. Install Docker
	 3. Install USB Backup
	 4. Install Flash Memory
	 5. Install Disk stats
	 6. Install APT Tool
	 7. Install ClamAV
	 8. Install Let's Encrypt
	 9. Install locate
	10. Install Reset Permissions
23. OMV-Extras
	1. Enable Docker repository
24. Disks
	1. Wipe the disk
	2. Edit the disk
		1. Advanced Power Management: 1 - Minimum power usage with standby
		2. Automatic Acoustic Management: Minimum performance, Minimum acoustic out
		3. Spindown time: 30 minutes
35. S.M.A.R.T.
	1. Enable
	2. Check interval: 43200
	3. Power mode: Standby
	4. Configure devices
	5. Configure Scheduled tests
36. RAID Management
	Nothing to do
37. File Systems
	1. Create the file system
	2. Mount the file system
38. Flash Memory
	1. Connect with SSH
	2. sudo vim /etc/fstab
	3. Comment swap partition
	4. Add noatime and nodiratime to /
39. User
	1. Add users
		1. Groups: sudo, ssh, users, docker and the group with the same name as the user
		2. Add the public key if needed
		3. After login, create the user directory in /home/$USER and chown -R $USER:$USER /home/$USER
40.  Group
	1. Create the $USER groups
Shared Folders
	1. Add a new share
		1. Name: As wanted
		2. Device: As wanted
		3. Path: Same of the name (recommended)
		4. Permissions: Administrator: read/write, Users: read/write, Others: read-only
	2. Create the folders in the shared path with SSH
		1. Media
			1. Config/
			2. Documents/
			3. Music
			4. Videos/
				1. Animations/
				2. Anime/
				3. Documentaries/
				4. Movies/
				5. TV Shows/
Services
	Antivirus
		1. Enable the plugin
		2. Configure the plugin
		3. Configure Scheduled Jobs
			1. Do nothing when threat found
			2. Send an email
	Apt Tool
		1. Install zsh
		2. Install fail2ban
		3. Install git
		4. Install docker-ce
		5. Install docker-compose
	Docker
		1. Enable the plugin
		2. Add the following images:
			1. https://hub.docker.com/r/rednoah/filebot/
			2. https://hub.docker.com/r/emby/embyserver/
			or https://github.com/IrosTheBeggar/mStream/blob/master/Dockerfile
			or https://hub.docker.com/r/wernight/mopidy / https://hub.docker.com/r/whhoesj/mopidy
			or https://hub.docker.com/r/0xcaff/koel/dockerfile / https://hub.docker.com/r/binhex/arch-koel/dockerfile / https://hub.docker.com/r/ampache/ampache
			3. https://hub.docker.com/r/linuxserver/beets/
			4. https://hub.docker.com/r/romancin/rutorrent-flood/
			5. https://hub.docker.com/r/couchpotato/couchpotato /
radarr / flexget
			6. https://hub.docker.com/r/linuxserver/sickrage / https://hub.docker.com/r/sickchill/sickchill
or https://github.com/pymedusa/Medusa
			7. NextCloud
		3. Use this ONLY to monitor if everything is working.
	FTP
		1. Do nothing
	LetsEncrypt
		1. 
	Locate
		1. Do nothing
	NFS
		1. Enable the plugin
		2. Number of servers: 4
		3. Shares:
			1. Shared folder: As wanted
			2. Client: 192.168.1.0/24
			3. Privilege: Read/Write
			4. Extra options: By default
	Rsync
		1. Do nothing
	SMB/CIFS
		1. Enable
		2. Shares
			 1. Shared folder: As wanted
			 2. Public: Guest allowed
			 3. Read only: Off
			 4. Browseable: On
			 5. Inherit ACLs: On
			 6. Inherit permissions: On
			 7. Recycle bin: Off
			 8. Hide dot files: On
			 9. Extended attributes: Off
			10. Store DOS attributes: Off
			11. Hosts allow: As wanted
			12. Hosts deny: As wanted
			13. Audit: Off
	SSH
		1. Enable
		2. Port: 22
		3. Permit root login: Off
		4. Password authentification: On
		5. Public key authentification: On
		6. TCP forwarding: Off
		7. Compression: On
		8. Extra options: Empty
	USB Backup
		1. Add the devices
			13: Extra options: empty
Connect the hard drive
```
